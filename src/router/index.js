import Vue from 'vue'
import Router from 'vue-router'
import store from '../store'
import Home from '@/components/Home'
import Login from '@/components/Auth/Login'
import Courses from '@/components/Course'
import AddCourse from '@/components/AddCourse'
import CourseInfo from '@/components/CourseInfo'
import Registration from '@/components/Auth/Registration'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      beforeEnter (to, from, next) {
        store.getters.checkUser ? next() : next('/login')
      }
    },
    {
      path: '/courses',
      name: 'courses',
      component: Courses
    },
    {
      path: '/addcourse',
      name: 'addcourse',
      component: AddCourse
    },
    {
      path: '/courseInfo',
      name: 'courseInfo',
      component: CourseInfo
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/registration',
      name: 'registration',
      component: Registration
    }
  ]
})
