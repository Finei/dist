// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuelidate from 'vuelidate'
import Uimini from 'uimini/dist/css/uimini.css'
import iView from 'iview'
import 'iview/dist/styles/iview.css'

import App from './App'
import router from './router'
import store from './store'

import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'
import 'firebase/storage'

Vue.use(
  Vuelidate,
  Uimini,
  iView
)
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>',
  created () {
    var config = {
      apiKey: 'AIzaSyDjOngsNHyvtI-dU2CmVmxXuL8mGiPeNck',
      authDomain: 'distc-717b4.firebaseapp.com',
      databaseURL: 'https://distc-717b4.firebaseio.com',
      projectId: 'distc-717b4',
      storageBucket: 'distc-717b4.appspot.com',
      messagingSenderId: '345823112381'
    }
    firebase.initializeApp(config)

    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.$store.dispatch('loggedUser', user)
      }
      this.$store.dispatch('loadUsers')
      this.$store.dispatch('loadCources')
    })
  }
})
