import firebase from 'firebase/app'

import User from './user_help'
import UserDB from './userdb_help'

export default {
  state: {
    user: null,
    users: []
  },
  mutations: {
    setUser (state, payload) {
      state.user = payload
    },
    loadUsers (state, payload) {
      state.users = payload
    }
  },
  actions: {
    async registerUser ({commit}, {email, password, isAdmin}) {
      commit('clearError')
      commit('setLoading', true)
      try {
        const user = await firebase.auth().createUserWithEmailAndPassword(email, password)
        const newUser = new UserDB(
          user.user.uid,
          email,
          isAdmin
        )
        await firebase.database().ref('users').push(newUser)
        commit('setUser', new User(user.user.uid, isAdmin))
        commit('setLoading', false)
      } catch (error) {
        commit('setLoading', false)
        commit('setError', error.message)
        throw error
      }
    },
    async loginUser ({commit}, {email, password}) {
      commit('clearError')
      commit('setLoading', true)
      try {
        const user = await firebase.auth().signInWithEmailAndPassword(email, password)
        const userdb = await firebase.database().ref('users').once('value')
        const users = userdb.val()
        var isAdmin = false
        Object.keys(users).forEach(key => {
          if (user.user.uid === users[key].id) {
            isAdmin = users[key].isAdmin !== null ? users[key].isAdmin : false
          }
        })
        commit('setUser', new User(user.user.uid, isAdmin))
        commit('setLoading', false)
      } catch (error) {
        commit('setLoading', false)
        commit('setError', error.message)
        throw error
      }
    },
    async loggedUser ({commit}, payload) {
      commit('clearError')
      commit('setLoading', true)
      try {
        const userdb = await firebase.database().ref('users').once('value')
        const users = userdb.val()
        var isAdmin = false
        Object.keys(users).forEach(key => {
          if (payload.uid === users[key].id) {
            isAdmin = users[key].isAdmin !== null ? users[key].isAdmin : false
          }
        })
        commit('setUser', new User(payload.uid, isAdmin))
        commit('setLoading', false)
      } catch (error) {
        commit('setLoading', false)
        commit('setError', error.message)
        throw error
      }
    },
    logoutUser ({commit}) {
      firebase.auth().signOut()
      commit('setUser', null)
    },
    async loadUsers ({commit}, payload) {
      commit('clearError')
      commit('setLoading', true)
      try {
        const user = await firebase.database().ref('users').once('value')
        const users = user.val()
        const usersArray = []
        Object.keys(users).forEach(key => {
          const c = users[key]
          usersArray.push(
            new UserDB(
              c.id,
              c.email
            )
          )
        })
        commit('loadUsers', usersArray)
        commit('setLoading', false)
      } catch (error) {
        commit('setLoading', false)
        commit('setError', error.message)
        throw error
      }
    }
  },
  getters: {
    user (state) {
      return state.user
    },
    checkUser (state) {
      return state.user !== null
    },
    users (state) {
      return state.users
    }
  }
}
