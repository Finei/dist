import Course from './course_help'
import firebase from 'firebase/app'

export default {
  state: {
    courses: [],
    currentCourse: [],
    editCourse: []
  },
  mutations: {
    newCourse (state, payload) {
      state.courses.push(payload)
    },
    loadCources (state, payload) {
      state.courses = payload
    },
    currentCourse (state, payload) {
      state.currentCourse = payload
    },
    editCourse (state, payload) {
      state.editCourse = payload
    }
  },
  actions: {
    async loadCources ({commit}) {
      commit('clearError')
      commit('setLoading', true)
      try {
        const course = await firebase.database().ref('courses').once('value')
        const courses = course.val()
        const coursesArray = []
        Object.keys(courses).forEach(key => {
          const c = courses[key]
          coursesArray.push(
            new Course(
              c.title,
              c.description,
              c.subcurses,
              c.users,
              c.id,
              c.user,
              key
            )
          )
        })
        commit('loadCources', coursesArray)
        commit('setLoading', false)
      } catch (error) {
        commit('setLoading', false)
        commit('setError', error.message)
        throw error
      }
    },
    async newCourse ({commit, getters}, payload) {
      commit('clearError')
      commit('setLoading', true)
      try {
        const newCourse = new Course(
          payload.title,
          payload.description,
          payload.subcurses,
          payload.users,
          payload.id,
          getters.user.id
        )
        const courses = await firebase.database().ref('courses').push(newCourse)
        // console.log(courses)
        commit('newCourse', {
          ...newCourse,
          id: courses.key
        })
        commit('setLoading', false)
      } catch (error) {
        commit('setLoading', false)
        commit('setError', error.message)
        throw error
      }
    },
    async deleteCourse ({commit}, id) {
      commit('clearError')
      commit('setLoading', true)
      try {
        await firebase.database().ref('courses').child(id).remove()
        commit('setLoading', false)
      } catch (error) {
        commit('setLoading', false)
        commit('setError', error.message)
        throw error
      }
    },
    async editdbCourse ({commit}, course) {
      commit('clearError')
      commit('setLoading', true)
      try {
        var key = course.key
        var title = course.title
        var description = course.description
        var subcurses = course.subcurses
        var users = course.users
        await firebase.database().ref('courses').child(key).update({
          title,
          description,
          subcurses,
          users
        })
        commit('setLoading', false)
      } catch (error) {
        commit('setLoading', false)
        commit('setError', error.message)
        throw error
      }
    },
    editCourse ({commit}, payload) {
      commit('editCourse', payload)
    },
    currentCourse ({commit}, payload) {
      commit('currentCourse', payload)
    }
  },
  getters: {
    courses (state, getters) {
      return state.courses.filter(item => {
        console.log(getters.user)
        return getters.user.isAdmin || item.user.includes(getters.user !== null ? getters.user.id : 0)
      })
    },
    currentCourse (state) {
      return state.currentCourse
    },
    editCourse (state) {
      return state.editCourse
    }
  }
}
