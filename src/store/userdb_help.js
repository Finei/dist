export default class UserDB {
  constructor (id, email, isAdmin) {
    this.id = id
    this.email = email
    this.isAdmin = isAdmin
  }
}
