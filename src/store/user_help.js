export default class User {
  constructor (id, isAdmin) {
    this.id = id
    this.isAdmin = isAdmin
  }
}
