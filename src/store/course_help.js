export default class Course {
  constructor (
    title,
    description,
    subcurses,
    users,
    id,
    user = null,
    key = null
  ) {
    this.title = title
    this.description = description
    this.subcurses = subcurses
    this.users = users
    this.id = id
    this.user = user
    this.key = key
  }
}
